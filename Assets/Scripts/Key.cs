using System;
using System.Collections;
using System.Collections.Generic;
using EasyButtons;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Key : Platform {

    public static Action<string> onKeyPress;
    
    [SerializeField] string keyCode;

    [SerializeField] GameObject labelToSpawn;
    [SerializeField] bool spawnLabel = true;

    Material mat;
    Color baseColor;
    Color highlightColor;

    protected override void Awake() {
        base.Awake();
        GenerateColors();
        if (spawnLabel) {
            GameObject labelGo = Instantiate(labelToSpawn, transform);
            TextMeshPro tm = labelGo.GetComponent<TextMeshPro>();
            if (keyCode.Length > 1) {
                tm.text = keyCode.Substring(0, 2);
            }
            else {
                tm.text = keyCode;
            }
        }
    }


    protected override void OnValidate() {
        base.OnValidate();
        gameObject.name = $"{keyCode}_{colorKey}";
    }



    void OnCollisionEnter(Collision col) {
        if (base.IsActiveColour()) {
            SetPressed(true);
            onKeyPress?.Invoke(keyCode);
        }
    }

    void OnCollisionExit(Collision col) {
        SetPressed(false);
    }

    
    void GenerateColors() {
        mat = GetComponent<Renderer>().material;
        baseColor = mat.color;
        Color.RGBToHSV(baseColor, out float h, out float s, out float v);
        v += 0.5f;
        s -= 0.3f;
        highlightColor = Color.HSVToRGB(h, s, v);
    }
    
    public void SetPressed(bool value) {
        //SetHeight(value);
        SetColor(value);
    }

    void SetColor(bool value) {
        if (mat == null) {
            return;
        }
        mat.color = value ? highlightColor : baseColor;
    }



}
