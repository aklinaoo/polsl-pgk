using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Switcher : ColorElement {
    
    [SerializeField] float unpressedHeight;
    [SerializeField] float pressedHeight = -0.24f;

    void Start() {
        ColorManager.ColorSet set = ColorManager.Instance.GetColorSet(colorKey);
        set.SetColour(GetMaterial(), true);
    }

    protected override void OnValidate() {
        base.OnValidate();
        gameObject.name = $"{colorKey}_switcher";
    }
    
    void OnCollisionEnter(Collision col) {
        SetPressed(true);
    }

    protected override void OnActiveChange(bool value) {
        //base.OnActiveChange(value);
        SetPressed(value);
    }

    public void SetPressed(bool value) {
        SetHeight(value);
        if (value) {
            ColorManager.Instance.SetColor(colorKey);
        }
    }


    void SetHeight(bool value) {
        Vector3 localPos = transform.localPosition;
        transform.localPosition = new Vector3(
            localPos.x,
            value ? pressedHeight : unpressedHeight,
            localPos.z
        );
    }
}
