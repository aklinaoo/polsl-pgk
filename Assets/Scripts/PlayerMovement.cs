using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using RoboRyanTron.SearchableEnum;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour {
   
   enum CamerType { Free = 0, Keyboard = 1, KeyboardKick = 2, Screen = 3 }
   
   public float movementSpeed = 10;
   [NamedArray(typeof(CamerType))]
   [SerializeField] CinemachineVirtualCameraBase[] vCams;
   [SerializeField] Transform lookCam;
   [SerializeField] GameObject parachute;
   CharacterController controller;

   const float smoothingFacing = 0.1f;
   const string hAxis = "Horizontal";
   const string vAxis = "Vertical";

   [SearchableEnum] [SerializeField] 
   KeyCode parachuteKey = KeyCode.LeftShift;
   [SearchableEnum] [SerializeField]
   KeyCode plungeKey = KeyCode.LeftControl;

   [SerializeField] float minDownSpeed = 1.0f;
   [SerializeField] float jumpImpulse = 20.0f;
   [SerializeField] float accelImpulse = 20.0f;
   [SerializeField] float jumpStrength = 1.0f;
   [SerializeField] [Range(0, 1)] float breakingStrength = 0.5f;
   [SerializeField] [Range(1, 2)] float accelStrength = 1.5f;
   float gravityForce = -9.81f;
   float jumpSpeed;

   enum JumpState { GROUNDED, JUMP_ACCELERATING, JUMP_SLOWING  }
   JumpState currentState;
   
   void Awake() {
      Cursor.lockState = CursorLockMode.Locked;
      controller = GetComponent<CharacterController>();
      currentState = JumpState.GROUNDED;
      parachute.SetActive(false);
      SetCam(CamerType.Screen);
   }

   void Update() {
      HorizontalPlaneMove();
      LoopJumpCheck();
      controller.Move(Vector3.up * (jumpSpeed * Time.deltaTime));
   }

   public void SetScreenView(float time) {
      SetCam(CamerType.Screen);
     DOVirtual.DelayedCall(time, () => SetCam(CamerType.Keyboard));
   }
   
   void LoopJumpCheck() {
      switch (currentState) {
         case JumpState.GROUNDED:
            GroundedState();
            break;
         case JumpState.JUMP_ACCELERATING:
            RisingJumpState();
            break;
         case JumpState.JUMP_SLOWING:
            FallingJumpState();
            break;
      }
   }
   
   void SetState(JumpState state) {
      switch (state) {
         case JumpState.GROUNDED:
            TouchGround();
            break;
         case JumpState.JUMP_ACCELERATING:
            StartJump();
            break;
         case JumpState.JUMP_SLOWING:
            SlowingJumpStart();
            break;
      }
      currentState = state;
   }

   #region  STATES

   void GroundedState() {
      if (Input.GetKeyDown(KeyCode.Space)) {
         SetState(JumpState.JUMP_ACCELERATING);
      }
   }

   void StartJump() {
      SetCam(CamerType.Keyboard);
      jumpSpeed = jumpImpulse;
   }

   void RisingJumpState() {
      jumpSpeed += jumpStrength * Time.deltaTime;
      if (Input.GetKeyUp(KeyCode.Space)) {
         SetState(JumpState.JUMP_SLOWING);
      }
   }

   void SlowingJumpStart() {
      //jumpSpeed = 0;
   }

   void FallingJumpState() {
      if (Input.GetKeyDown(plungeKey) && jumpSpeed < 0) {
         jumpSpeed -= accelImpulse;
      }
      else if (Input.GetKey(plungeKey)  && jumpSpeed < 0) {
         SetCam(CamerType.KeyboardKick);
         jumpSpeed += gravityForce * accelStrength * Time.deltaTime;
      }
      else if (Input.GetKey(parachuteKey)  ) {
         SetCam(CamerType.Keyboard);
         parachute.SetActive(true);
         if (jumpSpeed < 0) {
            jumpSpeed = Mathf.Min(
               jumpSpeed * breakingStrength,
               -minDownSpeed);
         }
         else {
            jumpSpeed += gravityForce * accelStrength * Time.deltaTime;
         }
      }
      else {
         SetCam(CamerType.Keyboard);
         parachute.SetActive(false);
         jumpSpeed += gravityForce * Time.deltaTime;
      }
      
      if (controller.isGrounded) {
         SetState(JumpState.GROUNDED);
      }
   }

   void TouchGround() {
      jumpSpeed = -1f;
      parachute.SetActive(false);
      SetCam(CamerType.Keyboard);
   }


   #endregion
   
 
   void SetCam(CamerType type) {
      for (int i = 0; i < vCams.Length; i++) {
         vCams[i].gameObject.SetActive(i == (int)type);
      }
   }

   void HorizontalPlaneMove() {
      float lr = Input.GetAxisRaw(hAxis);
      float ud = Input.GetAxisRaw(vAxis);

      Vector3 dir = new Vector3(lr, 0, ud).normalized;

      if (dir.sqrMagnitude >= Mathf.Epsilon) {
         //Debug.DrawLine(transform.position, transform.position + dir, Color.red);
         Vector3 camForward = new Vector3(lookCam.forward.x, 0, lookCam.forward.z).normalized;
         //Debug.DrawLine(transform.position, transform.position + camForward, Color.blue);
         Vector3 moveDir = Quaternion.LookRotation(dir, Vector3.up)
                           * camForward;
         Debug.DrawLine(transform.position, transform.position + moveDir, Color.green);
         transform.forward = Vector3.Lerp(transform.forward, moveDir, smoothingFacing);
         CollisionFlags val = controller.Move(moveDir * movementSpeed * Time.deltaTime);
      }
   }

}
