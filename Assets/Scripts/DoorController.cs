﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DG.Tweening;
using JetBrains.Annotations;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

public class DoorController : MonoBehaviour {

    [SerializeField] string target = "TEST";
    [SerializeField] ScreenDisplay display;
    [SerializeField] TextMeshPro keywordDisplay;
    [SerializeField] int diff = 4;
    
    public UnityEvent onCorrect;
    
    void Awake() {
        display.inputComplete += ParseAnswer;
        NewQuestion();
    }

    string GenerateTarget() {
        char[] targetEls = target.ToCharArray();
        List<char> newGeneration = new List<char>();
        
        for (int i = 0; i < diff; i++) {
            int attempts = 0;
            int newI = -1;
            do {
                attempts++;
                newI = Random.Range(0, targetEls.Length);
            } while (newGeneration.Contains(targetEls[newI]) || attempts < 100);
            newGeneration.Add(targetEls[newI]);
        }

        return new string(newGeneration.ToArray());
    }
    
    [Button()]
    void NewQuestion() {
        keywordDisplay.text = GenerateTarget();
        string question = keywordDisplay.text;
        display.SetDisplay("");
    }

    void ParseAnswer(string input) {
        bool correct = string.Equals(keywordDisplay.text, input); 
        if (correct) {
            onCorrect?.Invoke();
        }
        display.SetDisplay(correct ? "Correct" : "Wrong");
        DOVirtual.DelayedCall(3.0f, NewQuestion);
    }

}