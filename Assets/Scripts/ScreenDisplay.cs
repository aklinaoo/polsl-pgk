using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScreenDisplay : MonoBehaviour {

    public Action<string> inputComplete;
    
    [SerializeField] TextMeshPro textDisplay;

    void OnEnable() {
        Key.onKeyPress += ParseInput;
    }

    void OnDisable() {
        Key.onKeyPress -= ParseInput;
    }

    public void ParseInput(string val) {
        switch(val){
            case "Enter":
                Evaluate();
                break;
            case "Backspace":
                RemoveLast();
                break;
            default:
                textDisplay.text += val;
                break;
        }
    }

    void RemoveLast() {
        string current = textDisplay.text;
        if (current[current.Length - 1] == '=') {
            Debug.Log("NO");
            return;
        }
        textDisplay.text = current.Substring(0, current.Length - 1);
    }

    void Evaluate() {
        inputComplete.Invoke(textDisplay.text);
    }

    public string GetDisplay() {
        return textDisplay.text;
    }
    
    public void SetDisplay(string value) {
        textDisplay.text = value;
    }

}
