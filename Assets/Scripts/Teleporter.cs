using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public GameObject player;
    public Transform teleportTarget;

    void OnTriggerEnter(Collider o)
    {
        player.transform.position = teleportTarget.transform.position;
    }
  
}
