﻿using System;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class ColorElement : MonoBehaviour {

    [Dropdown("colorLabels")]
    public string colorKey;

    [SerializeField] MeshRenderer rend;

    List<string> colorLabels {
        get {
            if (ColorManager.Instance != null) {
                return ColorManager.Instance.GetNames;
            }
            else {
                List<string> toRet = new List<string>();
                toRet.Add("green");
                toRet.Add("red");
                toRet.Add("blue");
                toRet.Add("black");
                toRet.Add("white");
                return toRet;
            }
        }
    }

    protected virtual void Awake() {
        ColorManager.Instance.onColorSwitched += CheckActive;
        Init();
        OnActiveChange(IsActiveColour());
    }

    void OnDestroy() {
        ColorManager.Instance.onColorSwitched -= CheckActive;
    }

    protected virtual void OnValidate() {

    }

    protected virtual void Init() {
        if (rend == null) {
            rend = GetComponent<MeshRenderer>();
        }
    }

    public ColorManager.ColorSet GetColorSet() {
        return ColorManager.Instance.GetColorSet(colorKey);
    }
    
    void CheckActive(ColorManager.ColorSet set) {
        if (set.name == colorKey) {
            OnActiveChange(true);
        }
        else {
            OnActiveChange(false);
        }
    }

    public Material GetMaterial() {
        return rend.material;
    }

    protected bool IsActiveColour() {
        return ColorManager.Instance.IsActive(this);
    }

    protected virtual void OnActiveChange(bool value) {
        ColorManager.Instance.SetMaterial(this);
    }

}