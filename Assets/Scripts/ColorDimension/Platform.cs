﻿using System.Collections;
using UnityEngine;

public class Platform : ColorElement {

    Collider myCollider;

    protected override void Init() {
        base.Init();
        myCollider = gameObject.GetComponent<Collider>();
    }
    
    protected override void OnActiveChange(bool value) {
        base.OnActiveChange(value);
        if (value) {
            myCollider.enabled = true;
        } else {
            myCollider.enabled = false;
        }

        ColorManager.ColorSet set = GetColorSet();
        
        if (set.killPlane || set.alwaysActive) {
            myCollider.enabled = true;
        }
    }


}
