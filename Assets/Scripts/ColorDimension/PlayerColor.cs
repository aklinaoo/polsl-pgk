﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using RoboRyanTron.SearchableEnum;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{

    [SerializeField]
    List<GameObject> colorMarkers;
    List<Material> markerMats;
    
    // color
    ColorManager colorManager => ColorManager.Instance;
    private Vector3 startPosition;

    [SearchableEnum]
    [SerializeField] KeyCode colorSwitch = KeyCode.Q;

    private void Start() {
        markerMats = colorMarkers
            .Select(x => x.GetComponent<Renderer>().material)
            .ToList();
        startPosition = this.transform.position;
        colorManager.SetFirstColour();
        SetMarkerColor();
    }

    void Update()
    {
        if (Input.GetKeyDown(colorSwitch))
        {
            switchColor();
        }
    }

    void SetMarkerColor() {
        foreach (Material m in markerMats) {
            colorManager.currentColor.SetColour(m, true);
        }
    }

    void switchColor()
    {
        colorManager.SetNextActiveColour();
        SetMarkerColor();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        if (body)
        {
            if(body.gameObject.tag == "Finish")
            {
                this.transform.position = startPosition;
                Debug.Log("You won!");
            }
            Platform platform = body.GetComponent<Platform>();
            if(platform)
            {
                if(platform.GetColorSet().killPlane)
                {
                    this.transform.position = startPosition;
                    Debug.Log("You lost!");
                }
            }
        }
    }
}
