﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaughtyAttributes;
using UnityEngine;



[CreateAssetMenu(menuName = "SO/ColorManager")]
public class ColorManager : yaSingleton.Singleton<ColorManager>
{
    [SerializeField] List<ColorSet> colorSets;
    
    public ColorSet currentColor {
        private set {
            bool switched = value.name != currentColorKey;
            currentColorKey = value.name;
            if (switched) {
                onColorSwitched?.Invoke(value);
            }
        }
        get => colorSets.FirstOrDefault(x =>x.name == currentColorKey);
    }

    [Dropdown("GetNames")]
    [SerializeField] string currentColorKey; 

    public event Action<ColorSet> onColorSwitched;

    public List<string> GetNames => colorSets.Select(x => x.name).ToList();

    public void SetMaterial(ColorElement el)
    {
        colorSets.FirstOrDefault(x => x.name == el.colorKey).SetColour(el.GetMaterial(), IsActive(el));
    }
    
    public bool IsActive(ColorElement el)
    {
        if (GetColorSet(el.colorKey).alwaysActive) {
            return true;
        }
        else if (el.colorKey == currentColor.name) {
            return true;
        }
        return false;
    }

    public void SetFirstColour() {
        currentColor = colorSets[0];
    }
    
    public void SetNextActiveColour() {
        currentColor = colorSets
            .Where(x=>x.switchTo)
            .SkipWhile(x => x.name != currentColor.name)
            .Skip(1).
            FirstOrDefault() ?? colorSets.First();
    }

    public void SetColor(string key) {
        currentColor = colorSets.FirstOrDefault(x => x.name == key);
    }

    public ColorSet GetColorSet(string key) {
        return colorSets.FirstOrDefault(x => x.name == key);
    }

    [System.Serializable]
    public class ColorSet {

        public string name => _name;
        [SerializeField] string _name;
        [ColorUsage(false)]
        [SerializeField] Color active;
        [ColorUsage(false)]
        [SerializeField] Color inactive;
        public bool switchTo = true;
        public bool killPlane;
        public bool alwaysActive;

        public void SetColour(Material currentMaterial, bool isActive) {
            Color c = isActive ? active : inactive;
            if (alwaysActive) {
                c = active;
            }

            if (isActive || alwaysActive) {
                c.a = 1.0f;
            }
            else {
                c.a = 0.7f;
            }

            currentMaterial.color = c;
        }

    }
}
