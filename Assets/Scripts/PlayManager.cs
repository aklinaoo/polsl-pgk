﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class PlayManager : MonoBehaviour {

    [SerializeField] ScreenDisplay display;
    [SerializeField] PlayerMovement player;
    [SerializeField] TextMeshProUGUI pointsDisplay;
    [SerializeField] RectTransform timer;
    [SerializeField] int charWeight = 5;
    [SerializeField] float solvingTimeLimit = 10.0f;
    EquationGenerator eq;

    int pointsCount {
        set {
            _pointCount = value;
            pointsDisplay.text = _pointCount.ToString();
        }
        get {
            return _pointCount;
        }
    }
    int _pointCount;
    Tween tw;
    
    void Awake() {
        eq = new EquationGenerator();
        display.inputComplete += ParseAnswer;
        pointsCount = 0;
        NewQuestion();
    }

    void NewQuestion() {
        eq.Generate();
        string question = eq.GetQuestion();
        display.SetDisplay(question);

        timer.anchorMin = Vector2.zero;
        tw = timer.DOAnchorMin(Vector2.right, solvingTimeLimit);
        tw.onComplete = () => ParseAnswer(display.GetDisplay());
    }

    void ParseAnswer(string input) {
        bool correct = eq.IsCorrect(input, out int difficulty);
        if (correct) {
            pointsCount += 
                (difficulty * charWeight) 
                + (int)((1.0f - timer.anchorMin.x)* solvingTimeLimit);
        }
        display.SetDisplay(correct ? "Correct" : "Wrong");
        DOVirtual.DelayedCall(3.0f, NewQuestion);
        player.SetScreenView(5.0f);
        tw?.Kill();
    }

    public class EquationGenerator {

        int[] values;

        public EquationGenerator() {
            values = new int[2];
        }
        
        public void Generate() {
            for (int i = 0; i < values.Length; i++) {
                values[i] = UnityEngine.Random.Range(-10, 10);
            }
        }

        public string GetQuestion() {
            StringBuilder sb = new StringBuilder();
            AddNum(ref sb, values[0]);
            for (int i = 1; i < values.Length; i++) {
                sb.Append("+");
                AddNum(ref sb, values[i]);
            }
            sb.Append("=");
            return sb.ToString();
        }

        void AddNum(ref StringBuilder sb, int num) {
            if (num < 0) {
                sb.Append('(');
                sb.Append(num);
                sb.Append(')');
            }
            else {
                sb.Append(num);    
            }
            
        }

        public bool IsCorrect(string fullString, out int difficulty ) {
            try {
                string answer = fullString.Remove(0, GetQuestion().Length);
                int result = int.Parse(answer);
                difficulty = answer.Length;
                return result == values.Sum();
            }
            catch {
                difficulty = 0;
                return false;
            }
        }

    }
}